<?php

namespace LaravelNonWwwRedirect;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class LaravelNonWwwRedirectMiddleware
 *
 * @package LaravelNonWwwRedirect
 */
class LaravelNonWwwRedirectMiddleware
{

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $host = $request->header('host');

        if (!Str::startsWith($host, 'www.')){
            $redirectHost = 'www.' . $host;
            $request->headers->set('host', $redirectHost);
            return new RedirectResponse($request->fullUrl(), 301);
        }

        return $next($request);
    }
}