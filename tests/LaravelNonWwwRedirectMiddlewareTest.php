<?php

namespace LaravelNonWwwRedirect\Tests;

use Faker\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use LaravelNonWwwRedirect\LaravelNonWwwRedirectMiddleware;
use Orchestra\Testbench\TestCase;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\HeaderBag;

/**
 * Class LaravelNonWwwRedirectMiddlewareTest
 * @covers LaravelNonWwwRedirectMiddleware
 * @package LaravelNonWwwRedirect\Tests
 */
class LaravelNonWwwRedirectMiddlewareTest extends TestCase
{
    /**
     * @var array
     */
    private $mocks = [];

    public function testNonWwwHostnameIsRedirected()
    {
        /**
         * Arrange
         */
        $domainNonWww = $this->faker()->domainName;
        $domainPlusWww = 'www.' . $domainNonWww;
        $domainHttp = 'http://' . $domainPlusWww;
        $expectedHttpStatus = 301;
        $this->mocks['Request']->shouldReceive('header')->once()->andReturn($domainNonWww);
        $this->mocks['HeaderBag']->shouldReceive('set')->once()->with('host', $domainPlusWww);
        $this->mocks['Request']->shouldReceive('fullUrl')->once()->andReturn($domainHttp);

        /**
         * Act
         */
        $class = new LaravelNonWwwRedirectMiddleware();
        $return = $class->handle($this->mocks['Request'], $this->mocks['Closure']);

        /**
         * Assert
         */
        Assert::assertInstanceOf(RedirectResponse::class, $return);
        Assert::assertSame($expectedHttpStatus, $return->getStatusCode());
        Assert::assertSame($domainHttp, $return->getTargetUrl());
    }

    public function testWwwHostnameIsNotRedirected()
    {
        /**
         * Arrange
         */
        $domainNonWww = $this->faker()->domainName;
        $domainPlusWww = 'www.' . $domainNonWww;
        $expectedReturn = $this->mocks['Closure']();
        $this->mocks['Request']->shouldReceive('header')->once()->andReturn($domainPlusWww);
        $this->mocks['HeaderBag']->shouldReceive('set')->never();
        $this->mocks['Request']->shouldReceive('fullUrl')->never();

        /**
         * Act
         */
        $class = new LaravelNonWwwRedirectMiddleware();
        $return = $class->handle($this->mocks['Request'], $this->mocks['Closure']);

        /**
         * Assert
         */
        Assert::assertSame($expectedReturn, $return);
    }


    /**
     * Return a faker object
     *
     * @return \Faker\Generator
     */
    private function faker()
    {
        return Factory::create();
    }

    public function setUp(): void
    {
        $this->mocks['Request'] = \Mockery::mock(Request::class);
        $this->mocks['HeaderBag'] = \Mockery::mock(HeaderBag::class);
        $randomOutput = $this->faker()->sentence;
        $this->mocks['Closure'] = function () use ($randomOutput) {return $randomOutput;};
        $this->mocks['Request']->headers = $this->mocks['HeaderBag'];
    }
}
